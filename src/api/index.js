import axios from 'axios'
import * as types from '../store/ItemModule/types'

export const getItem = (payload) => {
    return axios({
        url: `http://localhost:3006/item?activePage=${payload.activePage}&limit=${types.limit}`,
        method: 'GET'
    })
}

export const addItem = (name) => {
    return axios({
        url: `http://localhost:3006/item`,
        method: 'POST',
        data: {
            name
        }
    })
}

export const deleteItem = (id) => {
    return axios({
        url: `http://localhost:3006/item/${id}`,
        method: 'DELETE',
    })
}

export const updateItem = (payload) => {
    return axios({
        url: `http://localhost:3006/item/${payload.id}`,
        method: 'PUT',
        data: {
            name: payload.name
        }
    })
}

export const searchItem = (payload) => {
    console.log(payload,'PPPPPPPPPPPPPPPPPPPPP');
    return axios({
        url: `http://localhost:3006/search?textSeach=${payload.textSearch}&activePage=${payload.activePage}&limit=${types.limit}`,
        method: 'GET',
    })
}

export const registerItem = (payload) => {
    return axios({
        url: `http://localhost:3006/register`,
        method: 'POST',
        data: {
            nameRegister: payload.nameRegister,
            password: payload.password
        }
    })
}
