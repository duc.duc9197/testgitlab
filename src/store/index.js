import Vue from 'vue'
import Vuex from 'vuex'
import ItemModule from './ItemModule'

Vue.use(Vuex)

const logger = store => {
    store.subscribe((mutation, state) => {
        console.log(mutation);
        console.log(state);
    })
}

const store = new Vuex.Store({
    modules: {
        item: ItemModule
    },
    plugins: [logger]
})

export default store