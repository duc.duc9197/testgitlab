import * as types from './types'
import actions from './actions'

const DEFAULT_STATE = {
    listItem: [],
    isLoading: false,
    error: false,
    message: null,
    activePage: 1,
    totalPage: null
}

const ItemModule = {
    namespaced: true,
    state: () => DEFAULT_STATE,
    actions,
    mutations: {
        [types.GET_ITEM_REQUEST](state) {
            state.isLoading = true
        },
        [types.ADD_ITEM_REQUEST](state) {
            state.isLoading = true
        },
        [types.DELETE_ITEM_REQUEST](state) {
            state.isLoading = true
        },
        [types.UPDATE_ITEM_REQUEST](state) {
            state.isLoading = true
        },
        [types.SEARCH_ITEM_REQUEST](state) {
            state.isLoading = true
        },
        [types.REGISTER_ITEM_REQUEST](state) {
            state.isLoading = true
        },

        [types.GET_ITEM_SUCCESS](state, payload) {
            state.isLoading = false,
            state.listItem = payload.data,
            state.activePage = payload.activePage,
            state.totalPage = payload.totalPage
        },
        [types.SEARCH_ITEM_SUCCESS](state, payload) {
            state.isLoading = false,
            state.listItem = payload.data,
            state.activePage = payload.activePage,
            state.totalPage = payload.totalPage
        },
        [types.GET_ITEM_SUCCESS](state, payload) {
            state.isLoading = false,
            state.listItem = payload.data,
            state.activePage = payload.activePage,
            state.totalPage = payload.totalPage
        },
        [types.ADD_ITEM_SUCCESS](state) {
            state.isLoading = false    
        },
        [types.DELETE_ITEM_SUCCESS](state) {
            state.isLoading = false    
        },
        [types.UPDATE_ITEM_SUCCESS](state) {
            state.isLoading = false    
        },

        [types.GET_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        },
        [types.ADD_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        },
        [types.DELETE_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        },
        [types.UPDATE_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        },
        [types.SEARCH_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        },
        [types.REGISTER_ITEM_FAILURE](state, error) {
            state.error = true,
            state.message = error.message
        }
    }
}

export default ItemModule