import * as types from './types'
import * as api from '../../api/index'

const actions = {
    async [types.GET_ITEM_REQUEST]({ commit }, payload) {
        commit(types.GET_ITEM_REQUEST)
        try {
            const res = await api.getItem(payload)
            commit(types.GET_ITEM_SUCCESS, res.data)
        } catch (error) {
            commit(types.GET_ITEM_FAILURE, error)
        }
    },

    async [types.ADD_ITEM_REQUEST]({ commit, dispatch }, name) {
        commit(types.ADD_ITEM_REQUEST)
        try {
            await api.addItem(name)
            commit(types.ADD_ITEM_SUCCESS)
            dispatch(types.GET_ITEM_REQUEST, { activePage: 1 })
        } catch (error) {
            commit(types.ADD_ITEM_FAILURE, error)
        }
    },

    async [types.DELETE_ITEM_REQUEST]({ commit, dispatch }, id) {
        commit(types.DELETE_ITEM_REQUEST)
        try {
            await api.deleteItem(id)
            commit(types.DELETE_ITEM_SUCCESS)
            dispatch(types.GET_ITEM_REQUEST, { activePage: 1 })
        } catch (error) {
            commit(types.DELETE_ITEM_FAILURE, error)
        }
    },

    async [types.UPDATE_ITEM_REQUEST]({ commit, dispatch }, payload) {
        commit(types.UPDATE_ITEM_REQUEST)
        try {
            await api.updateItem(payload)
            commit(types.UPDATE_ITEM_SUCCESS)
            dispatch(types.GET_ITEM_REQUEST, { activePage: 1 })
        } catch (error) {
            commit(types.UPDATE_ITEM_FAILURE, error)
        }
    },

    async [types.SEARCH_ITEM_REQUEST]({ commit }, payload) {
        commit(types.SEARCH_ITEM_REQUEST)
        try {
            const res = await api.searchItem(payload)
            commit(types.SEARCH_ITEM_SUCCESS, res.data)
        } catch (error) {
            commit(types.SEARCH_ITEM_FAILURE, error)
        }
    },

    async [types.REGISTER_ITEM_REQUEST]({ commit }, payload) {
        commit(types.REGISTER_ITEM_REQUEST)
        try {
            const res = await api.registerItem(payload)
            commit(types.REGISTER_ITEM_SUCCESS, res.data)
            alert(`Hello ${res.data.name}`)
        } catch (error) {
            commit(types.REGISTER_ITEM_FAILURE, error)
        }
    }


}

export default actions